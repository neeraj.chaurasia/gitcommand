# Assignment-jenkins-2

## Must Do Section
#### [Git] Create a Jenkins job that will create a tag using Git plugin.
####    1. go to new item and create a project i have given my project name as “gitlabplugin” and select freestyle job and press ok
####    2. Then it will prompt to  configure page where we have to select git option in SCM and issue github url and set credentials.
####    3. Now go to the post build actions and select GIT PUBLISER and add tags and give tag versions give tag message if you want after write your github remote name and press save.
![alt text](images/jenkins1.png)
![alt text](images/jenkins4.png)
#### Maven Create a maven based jenkins job to build the code available in same repository.
![alt text](images/jenkins3.png)
![alt text](images/jenkins4.png)
#### [SSH Plugin] Run a script to find out top 5 files as major contributor to disk space.
![alt text](images/jenkins5.png)
#### [HTML Publisher]: Create a Jenkins job that will generate a HTML file for the commits happened in last 10 days and publishes it as HTML report
![alt text](images/jenkins6.png)
![alt text](images/jenkins7.png)
#### [Copy Artifacts]: Create 2 Jenkins job
#### ArtifactBuilder: This job will checkout the code corresponding to a static website and build artifact.
![alt text](images/jenkins9.png)
![alt text](images/artifact1.png)
#### ArtifactDeployer: This job will use the artifact built from previous job and deploys it to a target server. Also make sure that this job will not run when ArtifactBuilder job is running and vice versa.
##### sol.i will update it tomorrow



## Good to Do
#### Install Blue Ocean plugin and summarize features of it
![alt text](images/blueocean12.png)
#### Blue ocean is new user exprience that reduces the clutter and increses the clarity so in basically Blue Ocean is a new user interface for jenkins and provides an interactive view for jenkins Pipeline.
